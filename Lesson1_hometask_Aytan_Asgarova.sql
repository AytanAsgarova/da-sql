--Get customer name and customer id column where unit price is 707
SELECT CUSTOMER_NAME, CUSTOMER_ID
FROM sales_data 
WHERE UNIT_PRICE=707;
--Get Product brand and code which have been sold by female sale representetives
SELECT PRODUCT_BRAND, PRODUCT_CODE
FROM sales_data
WHERE GENDER='Female';
--Which category of products have beeen paid via Google Pay in Asia
SELECT Product_Category
FROM sales_data
WHERE PAYMENT_METHOD='Google Pay' AND SALES_REP_REGION='Asia';
--Which category of products have been purchased by Chinese customers in 2013
SELECT Product_Category
FROM sales_data
WHERE YEAR_='2013' AND CUSTOMER_COUNTRY='China';
--Which subcategory of products have been returned because of its defectiveness in 2022
SELECT Product_Subcategory
FROM sales_data
WHERE YEAR_='2022' AND Return_reason='Defective Item';
--Which brands have been sold in store and sorted by descending order
SELECT PRODUCT_BRAND, TOTAL_PRICE
FROM sales_data
WHERE Sales_channel='In-store'
Order by TOTAL_PRICE desc;
--Show sales representetives who have low income and their sales 
SELECT SALES_REP_NAME, TOTAL_PRICE
FROM sales_data
WHERE Income_level='low';
--To which cusomers and products discount has bee applied
SELECT CUSTOMER_NAME, PRODUCT_CODE
FROM sales_data
WHERE Discount=1;
--Show tracking numbers which are shipped by DHL in 2017
SELECT TRACKING_NUMBER
FROM sales_data
WHERE Shipping_company='DHL' and Year_=2017;
--Productid and its reasons which cancelled voluntary
SELECT PRODUCT_ID,CANCELLATION_REASON
FROM sales_data
WHERE Cancellation_category='Voluntary';